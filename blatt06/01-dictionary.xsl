<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/dictionary">
    <html>
      <head>
      <meta charset="UTF-8"/>
        <style>

        .header {
            position: absolute;
            left: 50%;

            transform: translate(-50%, 0%);
            font-size: 38px;
        }

        .second-header {
            width: 100%;
            text-align: center;

            font-size: 28px;
        }

        .main-content {
            position: absolute;
            top:12%;
            width: 60%;
            left: 20%;
        }

        .dict-table {
           
            width: 100%;
            text-align: center;
        }
        
        th {
            padding-bottom: 10px;
        }

        tr:nth-child(even) {
            background: #CCC
        }
        
        tr:nth-child(odd) {
            background: #FFF
        }

        td {
            height: 40px;
            white-space: nowrap;
        }

        .statistik {
            text-align: center;
        }
        </style>
      </head>
      <body>
      <p></p>
        <h2 class="header">Wörterliste</h2>

        <!-- <xsl:if test="count(eintrag[status='offen']) &gt; 2">
          <p>Bitte vergiss nicht, dir einen Einkaufswagen mitzunehmen!</p>
        </xsl:if> -->
<!-- 
        <h2>Offene Einkäufe (<xsl:value-of select="count(eintrag[status='offen'])" />)</h2>
          <ul>
            <xsl:for-each select="eintrag[status='offen']">
              <xsl:sort select="prio" order="ascending" />
              <li><xsl:value-of select="name" /> (<xsl:value-of select="menge" />)</li>
            </xsl:for-each>
          </ul>

          <h2><xsl:text>Erledigten Einkäufe</xsl:text> (<xsl:value-of select="count(eintrag[status!='offen'])" />)</h2>
          <ul>
            <xsl:for-each select="eintrag[status!='offen']">
              <li><xsl:value-of select="name" /> <xsl:value-of select="menge" /></li>
            </xsl:for-each>  
          </ul> -->

        <div class="main-content">
          <table class="dict-table">
            <tr>
              <th>Englisch</th>
              <th>Deutsch</th>
              <th>Kategorie</th>
            </tr>
            
            <xsl:for-each select="word">
              <xsl:sort select="@value" order="ascending" />
              <tr>
                <td><xsl:value-of select="@value" /></td>
                <td><xsl:value-of select="translation[@lang='DE']" /></td>
                <td>
                    <p>
                        <xsl:if test="category='Animal'">
                            (😸) 
                        </xsl:if>
                        <xsl:if test="category='Geography'">
                            (🌍) 
                        </xsl:if>
                        <xsl:if test="category='Food'">
                            (🍗) 
                        </xsl:if>
                        <xsl:value-of select="category" />
                    </p>
                </td>
              </tr>
            </xsl:for-each>
          </table>
          <div class="statistik">
            <h3 class="second-header">Statistik</h3>
            <p> Die Liste enthält insgesamt <b><xsl:value-of select="count(word)"/> Vokabeln</b></p>
            <p>Deutsch: <xsl:value-of select="count(word/translation[@lang='DE'])"/></p>
            <p>Französisch: <xsl:value-of select="count(word/translation[@lang='FR'])"/></p>
            <p>Latein: <xsl:value-of select="count(word/translation[@lang='LA'])"/></p>
          </div>
        </div>     
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>