
const mHideClassName = 'fade';

const mStationContainer = document.getElementById('station-container');
const mBoardingContainer = document.getElementById('boarding-container');
const mBackToStations = document.getElementById('return-button');

mBackToStations.addEventListener('click', ShowStationTable);

const mSearchBox = document.getElementById('station-search-box');
const mStationTable = document.getElementById('station-table');
const mSearchButton = document.getElementById('search-button');

mSearchButton.addEventListener('click', QueryTrainStations);

const mBoardingTable = document.getElementById('boarding-table');

async function QueryTrainStations(_)
{
    const searchTerm = mSearchBox.value;

    if(searchTerm === '')
    {
        ClearStationTable();
        return;
    }

    const response = await fetch('https://api.deutschebahn.com/freeplan/v1/location/'+searchTerm);
    const rawText = await response.text();
    
    CreateTrainStations(rawText);
}

function CreateTrainStations(rawText)
{
    const stations = JSON.parse(rawText);
    
    ClearStationTable();

    for (const station of stations) 
    {
        const container = document.createElement('li');
        const value = document.createElement('span');

        value.classList.add('station-item');
        value.innerHTML = station.name;

        container.appendChild(value);
        mStationTable.appendChild(container);

        container.addEventListener('click', _ => QueryBoardingInfos(station.id));
    }
}

async function QueryBoardingInfos(stationId)
{
    const response = await fetch('https://api.deutschebahn.com/freeplan/v1/arrivalBoard/' + stationId + '?date=' + new Date(Date.now()).toISOString().split('T')[0]);
    const rawText = await response.text();

    CreateBoardingTable(rawText);

    ShowBoardingInformations();
}

function ShowBoardingInformations()
{
    mStationContainer.classList.remove('fade-in');
    mStationContainer.classList.add(mHideClassName);
    mBoardingContainer.classList.add('fade-in');
    mBoardingContainer.classList.remove(mHideClassName);
}

function ShowStationTable(_)
{
    mBoardingContainer.classList.remove('fade-in');
    mBoardingContainer.classList.add(mHideClassName);
    mStationContainer.classList.add('fade-in');
    mStationContainer.classList.remove(mHideClassName);
}

function CreateBoardingTable(rawText)
{
    const boardingInfos = JSON.parse(rawText);

    mBoardingTable.replaceChild(document.createElement('tbody'), mBoardingTable.firstChild);

    for (const boardingInfo of boardingInfos) 
    {
        const row = mBoardingTable.insertRow(0);
        const name = row.insertCell(0);
        name.innerHTML = boardingInfo.name;
        
        const date = row.insertCell(1);
        date.innerHTML = boardingInfo.dateTime;

        const origin = row.insertCell(2);
        origin.innerHTML = boardingInfo.origin;
        
        const track = row.insertCell(3);
        track.innerHTML = boardingInfo.track;
    }
}

function ClearStationTable()
{
    while (mStationTable.lastElementChild) 
    {
        mStationTable.removeChild(mStationTable.lastElementChild);
    }
}