
const tfTitle = document.getElementById('titel');
const taEintrag = document.getElementById('eintrag');
const btPost = document.getElementById('fragen');

let elementToEdit = null;

window.addEventListener('DOMContentLoaded', onLoadWebEntriesWithFetch)
btPost.addEventListener('click', onPostClicked);

async function onLoadWebEntriesWithFetch(_)
{
    const response = await fetch('https://öä.eu/FH/posts.xml');
    const rawText = await response.text();
    createOnlineEntries(rawText);
}

async function onLoadWebEntriesWithXMLHttpRequest(_)
{
    const request = new XMLHttpRequest();
    request.open('GET','https://öä.eu/FH/posts.xml');

    request.addEventListener('load', (_) => 
    {
        if (request.status >= 200 && request.status < 300) 
        {
            createOnlineEntries(request.responseText);
        } else 
        {
           console.warn(request.statusText, request.responseText);
        }
     });

    request.send();
}

function createOnlineEntries(rawXmlText)
{
    console.log(rawXmlText);
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(rawXmlText,"text/xml");

    const onlinePosts = xmlDoc.getElementsByTagName('post');
    
    const sortedPosts = [...onlinePosts]
        .sort((a, b) => 
        {
            const aDate = parseDate(a.getElementsByTagName('datum')[0].innerHTML);
            const bDate = parseDate(b.getElementsByTagName('datum')[0].innerHTML);
            return aDate - bDate;
        });

    for (const post of sortedPosts) 
    {
        const title = post.getElementsByTagName('title')[0].innerHTML;
        const text = post.getElementsByTagName('content')[0].innerHTML;

        addJournalEntry( '[WEB:]' + title, text);
    }

}

function parseDate(rawDate)
{
    const dateParts = rawDate.split('.');
    const day = dateParts[0];
    const month = dateParts[1];
    const year = dateParts[2];

    return new Date(year, month-1, day);
}

function onPostClicked(_)
{
    if(elementToEdit === null)
    {
        addJournalEntry(tfTitle.value, taEintrag.value);
    }
    else
    {
        updateJournalEntry();
    }

    tfTitle.value = '';
    taEintrag.value = '';
}

function addJournalEntry(title, text)
{
    if(title !== '' && text !== '')
    {
        const template = document.getElementsByClassName('j-eintrag')[0];
        const newEntry = template.cloneNode(true);
    
        const titleElement = newEntry.getElementsByClassName('titel')[0];
        titleElement.innerHTML = title;
        titleElement.addEventListener('dblclick', onEditJournalEntry);
        
        const textElement = newEntry.getElementsByClassName('eintrag')[0];
        textElement.innerHTML = text;
    
        const container = document.getElementById('journal');
        container.appendChild(newEntry);
    }
}

const journalTitles = document.getElementsByClassName('titel');

for(const title of journalTitles)
{
    title.addEventListener('dblclick', onEditJournalEntry);
}

function onEditJournalEntry(e)
{
    const titleElement = (e.target||e.srcElement);
    tfTitle.value = titleElement.innerHTML;
    const parentElement = titleElement.parentElement;
    const textElement = parentElement.getElementsByClassName('eintrag')[0];
    taEintrag.value = textElement.innerHTML;
    onUpdateTextCount(null);

    elementToEdit = {
        titleElement : titleElement,
        textElement : textElement
    };

    btPost.value = 'Update!';
}

function updateJournalEntry()
{
    elementToEdit.titleElement.innerHTML = tfTitle.value;
    elementToEdit.textElement.innerHTML = taEintrag.value;
    btPost.value = 'Post!';
}


taEintrag.addEventListener('keyup', onUpdateTextCount);

function onUpdateTextCount(_)
{
    const counter = document.getElementsByClassName('counter')[0];
    
    counter.innerHTML = taEintrag.textLength;
}