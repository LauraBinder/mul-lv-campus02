
const tfTitle = document.getElementById('titel');
const taEintrag = document.getElementById('eintrag');
const btPost = document.getElementById('fragen');

let elementToEdit = null;

btPost.addEventListener('click', onPostClicked);

function onPostClicked(evt)
{
    if(elementToEdit === null)
    {
        addJournalEntry();
    }
    else
    {
        updateJournalEntry();
    }

    tfTitle.value = '';
    taEintrag.value = '';
}

function addJournalEntry()
{
    if(tfTitle.value !== '' && taEintrag.value !== '')
    {
        const template = document.getElementsByClassName('j-eintrag')[0];
        const newEntry = template.cloneNode(true);
    
        const title = newEntry.getElementsByClassName('titel')[0];
        title.innerHTML = tfTitle.value;
        title.addEventListener('dblclick', onEditJournalEntry);
        const text = newEntry.getElementsByClassName('eintrag')[0];
        text.innerHTML = taEintrag.value;
    
        const container = document.getElementById('journal');
        container.appendChild(newEntry);
    }
    
}

const journalTitles = document.getElementsByClassName('titel');

for(const title of journalTitles)
{
    title.addEventListener('dblclick', onEditJournalEntry);
}

function onEditJournalEntry(e)
{
    const titleElement = (e.target||e.srcElement);
    tfTitle.value = titleElement.innerHTML;
    const parentElement = titleElement.parentElement;
    const textElement = parentElement.getElementsByClassName('eintrag')[0];
    taEintrag.value = textElement.innerHTML;
    onUpdateTextCount(null);

    elementToEdit = {
        titleElement : titleElement,
        textElement : textElement
    };

    btPost.value = 'Update!';
}

function updateJournalEntry()
{
    elementToEdit.titleElement.innerHTML = tfTitle.value;
    elementToEdit.textElement.innerHTML = taEintrag.value;
    btPost.value = 'Post!';
}


taEintrag.addEventListener('keyup', onUpdateTextCount);

function onUpdateTextCount(evt)
{
    const counter = document.getElementsByClassName('counter')[0];
    
    counter.innerHTML = taEintrag.textLength;
}